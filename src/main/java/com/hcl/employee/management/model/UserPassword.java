package com.hcl.employee.management.model;

public class UserPassword {

	private int passwordId;
	private int userId;
	private String userPasswords;
	private String timeStamp;

	public void setPasswordId(int passwordId) {
		this.passwordId = passwordId;
	}
	
	public int getPasswordId() {
		return passwordId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPasswords;
	}

	public void setUserPassword(String userPassword) {
		this.userPasswords = userPassword;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

}
