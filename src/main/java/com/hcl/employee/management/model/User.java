package com.hcl.employee.management.model;

import java.util.List;

public class User {

	private String password;
	private String confirmPassword;
	private boolean isFirstTimeSignIn;
	private List<UserPassword> userPassword;
	private int id;
	private String continent;
	private String country;
	private String city;
	private String state;
	private String firstName;
	private String email;
	private String lastName;
	private int zipCode;
	private String mobile;
	private String organization;

	public String getPassword() {
		return password;
	}

	public void setPassword(String mPassword) {
		this.password = mPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String mConfirmPassword) {
		this.confirmPassword = mConfirmPassword;
	}

	public boolean isFirstTimeSignIn() {
		return isFirstTimeSignIn;
	}

	public void setFirstTimeSignIn(boolean misFirstTimeSignIn) {
		this.isFirstTimeSignIn = misFirstTimeSignIn;
	}

	public List<UserPassword> getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(List<UserPassword> mUserPassword) {
		this.userPassword = mUserPassword;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String mContinent) {
		this.continent = mContinent;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String mCountry) {
		this.country = mCountry;
	}

	public String getState() {
		return state;
	}

	public void setState(String mState) {
		this.state = mState;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String mCity) {
		this.city = mCity;
	}

	public int getId() {
		return id;
	}

	public void setId(int mId) {
		this.id = mId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String mFirstName) {
		this.firstName = mFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String mLastName) {
		this.lastName = mLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String mEmail) {
		this.email = mEmail;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mMobile) {
		this.mobile = mMobile;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int mZipCode) {
		this.zipCode = mZipCode;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String mOrganization) {
		this.organization = mOrganization;
	}

}
