package com.hcl.employee.management.model;

public class ResponseModel<T> {

	private Boolean status;
	private String message;
	private T data;
	private int errorCode;

	public Boolean getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Object getData() {
		return data;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public ResponseModel(Boolean status, String message, T data, int errorCode) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
		this.errorCode = errorCode;
	}

	public ResponseModel() {
	}

}
