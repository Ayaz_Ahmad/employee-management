package com.hcl.employee.management.service;

import javax.mail.MessagingException;

import com.hcl.employee.management.model.Mail;

public interface EmailService {

	/**
	 * This method is used to send email on user's emailID
	 * 
	 * @param email parameters like emailID, subject and description
	 * @throws MessagingException
	 */
	public boolean sendEmail(Mail email);

}
