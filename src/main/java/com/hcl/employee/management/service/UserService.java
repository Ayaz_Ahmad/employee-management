package com.hcl.employee.management.service;

import java.util.List;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserDto;

public interface UserService {
	/**
	 * This method is used to check if user is exists in database or not with
	 * EMAILID.
	 * 
	 * @param emailAddress
	 */
	public boolean isUserExistByEmail(String emailAddress);

	/**
	 * This method is used to check if user is valid sign-in.
	 * 
	 * @param email & password
	 * @return returns boolean.
	 */
	public boolean validateSignIn(String emailAddress, String password);

	/**
	 * This method is used to add user in database.
	 * 
	 * @param User
	 * @return returns UserDto.
	 */
	public UserDto addUser(User user);

	/**
	 * This method is used to get user from database with given User's id.
	 * 
	 * @param id
	 * @return returns User.
	 */
	public User findUserById(int id);

	/**
	 * This method is used to check if user is exists in database with given User's
	 * id.
	 * 
	 * @param id
	 * @return returns boolean.
	 */
	public Boolean isUserExist(int id);

	/**
	 * This method is used to update user in database.
	 * 
	 * @Param UserDto
	 * @return returns updated User.
	 */
	public User updateUser(UserDto user);

	/**
	 * This method is used to update password.
	 * 
	 * @param user
	 * @return returns boolean.
	 */
	public User updatePassword(User user);

	/**
	 * This method is used to delete user from database.
	 * 
	 * @Param userId
	 * @return returns deletedRows.
	 */
	public int deleteUserById(int id);

	/**
	 * This method is used to send password on user's email ID.
	 * 
	 * @Param email
	 * @return returns boolean.
	 */
	public boolean sendPasswordOnEmail(String email);

	public UserDto getUserDto(User user);

	/**
	 * This method is used to get all users from database.
	 * 
	 * @return returns List<User>.
	 */
	public List<User> findAllUsers();

	/**
	 * This method is used to get all users in database with by name of user.
	 * 
	 * @param name
	 * @return returns list of users with containing names.
	 */
	public List<User> getUserByName(String name);

	/**
	 * This method is used to get user from database with user's email ID.
	 * 
	 * @Param email
	 * @return returns User.
	 */
	public User getUserByEmail(String emailId);

	public User getUserFromDto(UserDto userDto);

	/**
	 * This method is used to check if user is first time signing-in.
	 * 
	 * @Param email & password
	 * @return returns boolean.
	 */
	boolean checkIsFirstTimeSignIn(String email, String password);

	/**
	 * This method is used to check if user is valid.
	 * 
	 * @param email & password
	 * @return returns boolean.
	 */
	boolean checkUserEmailAndPassword(String email, String password);

}
