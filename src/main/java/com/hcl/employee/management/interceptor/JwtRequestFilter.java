package com.hcl.employee.management.interceptor;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.hcl.employee.management.model.User;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.JwtTokenUtil;

/**
 * For any incoming request this Filter class gets executed. It checks if the
 * request has a valid JWT token. If it has a valid JWT Token then it sets the
 * Authentication in the context, to specify that the current user is
 * authenticated.
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	UserService userService;

	/**
	 * For any incoming request it checks if the request has a valid JWT token. If
	 * it has a valid JWT Token then it sets the Authentication in the context, to
	 * specify that the current user is authenticated.
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		final String jwtToken = request.getHeader("authorizationtoken");

		String email = null;
		if (jwtToken != null) {
			email = jwtTokenUtil.getEmailFromToken(jwtToken);
		}

		// Once we get the token validate it.
		if (email != null && userService.isUserExistByEmail(email) && jwtTokenUtil.validateToken(jwtToken)) {
			User user = userService.getUserByEmail(email);
			UserDetails userDetails = new org.springframework.security.core.userdetails.User(email,
					user.getUserPassword().get(user.getUserPassword().size() - 1).getUserPassword(), new ArrayList<>());
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, null, userDetails.getAuthorities());
			usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

		}
		chain.doFilter(request, response);
	}

}