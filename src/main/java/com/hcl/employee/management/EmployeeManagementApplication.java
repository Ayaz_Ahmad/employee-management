package com.hcl.employee.management;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.hcl.employee.management.serviceimpl.UserServiceImpl;
import com.hcl.employee.management.utils.AppConstant;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan("com.hcl.employee.management.repository")
public class EmployeeManagementApplication {

	private Set<String> defaultProduceAndConsume = new HashSet<>(Arrays.asList("application/json"));

	@Bean
	@Primary
	UserServiceImpl getServiceObj(Logger logger) {
		return new UserServiceImpl(logger);
	}

	/**
	 * First call initiate from here
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementApplication.class);
	}

	/**
	 * This method is used create Docket bean
	 * 
	 * @return Docket
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(AppConstant.DEFAULT_API_INFO)
				.produces(defaultProduceAndConsume).consumes(defaultProduceAndConsume);

	}

	/**
	 * This method is used to create RestTemplate bean
	 * 
	 * @return RestTemplate obj
	 */
	@Bean
	public RestTemplate getRestClient() {
		RestTemplate restClient = new RestTemplate(
				new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
		restClient.setInterceptors(
				Collections.singletonList((request, body, execution) -> execution.execute(request, body)));
		return restClient;
	}
}