package com.hcl.employee.management.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.model.ResponseModel;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserDto;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.AppConstant;
import com.hcl.employee.management.utils.JwtTokenUtil;

@RestController
public class UserController {

	private final Logger logger;

	@Autowired
	public UserController(final Logger logger) {
		this.logger = logger;
		logger.info(AppConstant.USERCONTROLLER_BEAN_INITIALIZED);
	}

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired(required = false)
	private UserService userSevice;

	@Autowired
	RestTemplate restTemplate;

	@PostMapping("/usersignup")
	public ResponseEntity<Object> userSignup(@Valid @RequestBody UserDto userDto) {

		User user = userSevice.getUserFromDto(userDto);

		if (userSevice.isUserExistByEmail(userDto.getEmail())) {
			logger.warn(AppConstant.USER_WITH_EMAIL_EXIST, userDto.getEmail());
			return new ResponseEntity<>(
					new ResponseModel<Object>(false, AppConstant.USER_WITH_EMAIL_EXIST, null, 0),
					HttpStatus.BAD_REQUEST);

		}
		else 
		{
			UserDto resultUserDto = userSevice.addUser(user);
			logger.info(AppConstant.USER_CREATED);
			return new ResponseEntity<>(new ResponseModel<Object>(true, AppConstant.USER_CREATED,
					jwtTokenUtil.generateToken(user.getEmail(), resultUserDto.getId()), 1), HttpStatus.CREATED);
		}

	}

	@PostMapping("/usersignin")
	public ResponseEntity<Object> userSignIn(@RequestBody User userRequest)  {

		if (userSevice.isUserExistByEmail(userRequest.getEmail())) {
			logger.info(AppConstant.USER_WITH_EMAIL_EXIST , userRequest.getEmail());
			if (userSevice.checkIsFirstTimeSignIn(userRequest.getEmail(), userRequest.getPassword())) {
				return new ResponseEntity<>(
						new ResponseModel<User>(false, AppConstant.USER_NEED_TO_CHANGE_PSRD, userRequest, 0),
						HttpStatus.CONTINUE);
			} else {
				if (userSevice.validateSignIn(userRequest.getEmail(), userRequest.getPassword())) {
					User user = userSevice.getUserByEmail(userRequest.getEmail());
					logger.info(AppConstant.USER_LOGIN_SUCCESS);
					return new ResponseEntity<>(
							new ResponseModel<Object>(true, AppConstant.USER_LOGIN_SUCCESS,
									jwtTokenUtil.generateToken(userRequest.getEmail(), user.getId()), 0),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<>(
							new ResponseModel<Object>(false, AppConstant.USER_ID_OR_PSRD_ERROR, null, 0),
							HttpStatus.UNAUTHORIZED);
				}
			}

		} else {
			logger.info(AppConstant.EMAIL_DOES_NOT_EXIST);
			return new ResponseEntity<>(
					new ResponseModel<User>(false, AppConstant.EMAIL_DOES_NOT_EXIST, userRequest, 0),
					HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("/getUserDetails")
	public ResponseEntity<ResponseModel<UserDto>> getUserDetails(
			@RequestHeader("authorizationtoken") String authorizationtoken) {
		String id = jwtTokenUtil.getIDFromToken(authorizationtoken);
		User user = userSevice.findUserById(Integer.parseInt(id));
		if (user != null) {
			UserDto userDto = userSevice.getUserDto(user);
			return new ResponseEntity<>(
					new ResponseModel<UserDto>(true, AppConstant.USER_FOUND, userDto, 1), HttpStatus.OK);
		} else {
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(" User not found in database ");
		}
	}

	/**
	 * This method is used for getting particular user details based on name
	 * 
	 * @param name
	 * @return ResponseEntity<T>
	 */
	@GetMapping("/getUserByName/{name}")
	public ResponseEntity<Object> getUserDetailsByName(@PathVariable("name") String name) {

		List<User> users = userSevice.getUserByName(name);
		if (users != null && !users.isEmpty()) 
		{
			users.forEach(user -> user.setPassword(""));
			logger.info(AppConstant.USER_FOUND);
			return new ResponseEntity<>(new ResponseModel<List<User>>(true, AppConstant.USER_FOUND, users, -1),
					HttpStatus.OK);
		} 
		else
		{
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(AppConstant.USERS_NOT_FOUND_IN_DATABASE);
		}

	}

	/**
	 * This method is used for getting all users details
	 * 
	 * @return ResponseEntity<T>
	 */
	@GetMapping("/getAllUsers")
	public ResponseEntity<ResponseModel<List<User>>> getAllUsers() {

		List<User> users = userSevice.findAllUsers();
		if (users == null) {
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(AppConstant.USER_NOT_FOUND_IN_DATABASE);

		}

		users.forEach(user -> user.setPassword(""));

		return new ResponseEntity<>(
				new ResponseModel<List<User>>(true, AppConstant.USER_FOUND, users, 1), HttpStatus.OK);

	}

	/**
	 * This method is used for getting particular user details based on id
	 * 
	 * @param userId
	 * @param authorizationtoken
	 * @return
	 */

	@GetMapping(value = "/restTemplate/getUserDetails")
	public ResponseEntity<ResponseModel<UserDto>> getUserDetailsWithRestTamplate(
			@RequestHeader("authorizationtoken") String authorizationtoken) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("authorizationtoken", authorizationtoken);
		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseModel<UserDto> response = restTemplate.exchange("http://localhost:8080/getUserDetails", HttpMethod.GET,
				entity, new ParameterizedTypeReference<ResponseModel<UserDto>>() {
				}).getBody();
		if (response == null) {
			throw new UserNotFoundException(AppConstant.USER_NOT_FOUND_IN_DATABASE);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	/**
	 * This method is used for updating user details
	 * 
	 * @param userDto
	 * @return ResponseEntity<T>
	 */
	@PostMapping("/updateUser")
	public ResponseEntity<Object> updateUserDetails(@RequestBody UserDto userDto) {
		User user;
		if (Boolean.TRUE.equals(userSevice.isUserExist(userDto.getId())))
		{
			user = userSevice.updateUser(userDto);
		} 
		else
		{
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(AppConstant.USER_NOT_FOUND_IN_DATABASE);
		}
		logger.info(AppConstant.USER_UPDATED_SUCCESSFULLY);
		return new ResponseEntity<>(new ResponseModel<User>(true, AppConstant.USER_UPDATED_SUCCESSFULLY, user, 1),
				HttpStatus.OK);

	}

	/**
	 * This method is used for deleting particular user based on id
	 * 
	 * @param userId
	 * @return ResponseEntity<T>
	 */
	@ExceptionHandler(UserNotFoundException.class)
	@GetMapping("/deleteUser/{userId}")
	public ResponseEntity<Object> deleteUserDetails(@PathVariable("userId") @Min(1) int userId)
	{
		if (Boolean.FALSE.equals(userSevice.isUserExist(userId))) 
		{
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(AppConstant.USER_NOT_FOUND_IN_DATABASE);
		}

		int deletedRows = userSevice.deleteUserById(userId);
		logger.info(AppConstant.USER_DELETED_SUCCESSFULLY);
		return new ResponseEntity<>(
				new ResponseModel<Object>(true, AppConstant.USER_DELETED_SUCCESSFULLY, null, deletedRows),
				HttpStatus.OK);
	}

	/**
	 * This method is used for changing password
	 * 
	 * @param userDto
	 * @return ResponseEntity<T>
	 */
	// Change Password
	@PostMapping("/changePassword")
	public ResponseEntity<Object> changePassword(@RequestBody UserDto userDto) 
	{
		if (!userDto.getPassword().equalsIgnoreCase(userDto.getConfirmPassword())) {
			logger.error(AppConstant.USER_PSRD_MUST_BE_SAME);
			throw new UserNotFoundException(AppConstant.USER_PSRD_MUST_BE_SAME);
		}

		User user = userSevice.getUserByEmail(userDto.getEmail());
		if (user == null) {
			logger.error(AppConstant.USER_NOT_FOUND_IN_DATABASE);
			throw new UserNotFoundException(AppConstant.USER_NOT_FOUND_IN_DATABASE);

		}
		userDto.setId(user.getId());
		user.setPassword(userDto.getConfirmPassword());
		user = userSevice.updatePassword(user);
		logger.info(AppConstant.USER_PSRD_UPDATED);
		return new ResponseEntity<>(
				new ResponseModel<Object>(true, AppConstant.USER_PSRD_UPDATED, user, user.getId()), HttpStatus.OK);
	}

	/**
	 * This method sends auto generated password on registered email id
	 * 
	 * @param userDto
	 * @return ResponseEntity<T>
	 */
	@PostMapping("/forgetPassword")
	public ResponseEntity<Object> forgetPassword(@RequestBody User user) {

		if (userSevice.isUserExistByEmail(user.getEmail())) {
			userSevice.sendPasswordOnEmail(user.getEmail());
			logger.info(AppConstant.SUCCESSFULLY_PSRD_SEND);
			return new ResponseEntity<>(
					new ResponseModel<Object>(true, AppConstant.SUCCESSFULLY_PSRD_SEND, null, 1), HttpStatus.OK);
		} else {
			logger.info(AppConstant.USER_WITH_EMAIL_NOT_EXIST);
			return new ResponseEntity<>(
					new ResponseModel<Object>(true, AppConstant.USER_WITH_EMAIL_NOT_EXIST, null, 0),
					HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("/getUserByEmail/{email}")
	public ResponseEntity<Object> multipleDatasourceDemo(@PathVariable("email") String email) {

		if (StringUtils.isBlank(email)) {
			throw new UserNotFoundException(AppConstant.EMAIL_MUST_NOT_BE_EMPTY);
		}

		User user = userSevice.getUserByEmail(email);
		if (user != null) {
			user.setPassword(null);
			return new ResponseEntity<>(new ResponseModel<User>(true, AppConstant.USER_FOUND, user, -1),
					HttpStatus.OK);

		} else {
			logger.error(AppConstant.EMAIL_DOES_NOT_EXIST);
			throw new UserNotFoundException(AppConstant.EMAIL_DOES_NOT_EXIST);
		}

	}

}
