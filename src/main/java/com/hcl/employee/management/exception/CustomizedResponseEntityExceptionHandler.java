package com.hcl.employee.management.exception;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hcl.employee.management.model.ResponseModel;
import com.hcl.employee.management.utils.AppConstant;

/**
 * This is common class for handling all exception in this project
 */

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	private final Logger empLogger;

	@Autowired
	public CustomizedResponseEntityExceptionHandler(final Logger logger) {
		this.empLogger = logger;
	}

	@ExceptionHandler({Exception.class})
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ResponseModel<Object> exceptionResponse = new ResponseModel<>(false, ex.getMessage(), null, 0);
		empLogger.error(AppConstant.EXCEPTION_MSG, ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	/*
	 * @ExceptionHandler(UserNotFoundException.class) public final
	 * ResponseEntity<Object> handleUserNotFoundExceptions(UserNotFoundException ex,
	 * WebRequest request) { ResponseModel<Object> exceptionResponse = new
	 * ResponseModel<>(true, ex.getMessage(), null, 0);
	 * empLogger.error(AppConstant.EXCEPTION_MSG, ex.getMessage()); return new
	 * ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
	 * 
	 * }
	 */

}
