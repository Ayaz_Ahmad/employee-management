package com.hcl.employee.management.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This class is called if user is not updated
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UserNotUpdatedException extends RuntimeException {

	private static final long serialVersionUID = -2655874589425824463L;

	public UserNotUpdatedException(String exception) {
		super(exception);
	}
}
