package com.hcl.employee.management.repository.dbmapper;

import org.apache.ibatis.annotations.Select;

public interface MultipleMapper {

	@Select("SELECT email_id FROM testtable")
	String getUserEmailId();

}
