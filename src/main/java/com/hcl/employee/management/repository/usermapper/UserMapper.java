package com.hcl.employee.management.repository.usermapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Many;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserPassword;
import com.hcl.employee.management.utils.QueryConstant;
import com.hcl.employee.management.utils.UserColumns;
import com.hcl.employee.management.utils.UserProperties;

@Mapper
public interface UserMapper {

	@Select(QueryConstant.SELECT_ALL_FROM_USER)
	@Results({ @Result(id = true, property = UserProperties.ID, column = UserColumns.ID),
			@Result(property = UserProperties.FIRSTNAME, column = UserColumns.FIRSTNAME),
			@Result(property = UserProperties.LASTNAME, column = UserColumns.LASTNAME),
			@Result(property = UserProperties.EMAIL, column = UserColumns.EMAIL),
			@Result(property = UserProperties.MOBILE, column = UserColumns.MOBILE),
			@Result(property = UserProperties.CONTINENT, column = UserColumns.CONTINENT),
			@Result(property = UserProperties.COUNTRY, column = UserColumns.COUNTRY),
			@Result(property = UserProperties.STATE, column = UserColumns.STATE),
			@Result(property = UserProperties.CITY, column = UserColumns.CITY),
			@Result(property = UserProperties.ZIPCODE, column = UserColumns.ZIPCODE),
			@Result(property = UserProperties.ORGANIZATION, column = UserColumns.ORGANIZATION),
			@Result(property = UserProperties.IS_FIRST_TIME_SIGNIN, column = UserColumns.IS_FIRST_TIME_SIGNIN) })
	public List<User> findAllUsers();

	@Insert(QueryConstant.INSERT_INTO_USER)
	@Options(useGeneratedKeys = true, keyProperty = UserProperties.ID)
	public int insertUser(User user);

	@Select(QueryConstant.SELECT_USER_BY_ID)
	@Results({ @Result(id = true, property = UserProperties.ID, column = UserColumns.ID),
			@Result(property = UserProperties.FIRSTNAME, column = UserColumns.FIRSTNAME),
			@Result(property = UserProperties.LASTNAME, column = UserColumns.LASTNAME),
			@Result(property = UserProperties.EMAIL, column = UserColumns.EMAIL),
			@Result(property = UserProperties.MOBILE, column = UserColumns.MOBILE),
			@Result(property = UserProperties.CONTINENT, column = UserColumns.CONTINENT),
			@Result(property = UserProperties.COUNTRY, column = UserColumns.COUNTRY),
			@Result(property = UserProperties.STATE, column = UserColumns.STATE),
			@Result(property = UserProperties.CITY, column = UserColumns.CITY),
			@Result(property = UserProperties.ZIPCODE, column = UserColumns.ZIPCODE),
			@Result(property = UserProperties.ORGANIZATION, column = UserColumns.ORGANIZATION),
			@Result(property = UserProperties.IS_FIRST_TIME_SIGNIN, column = UserColumns.IS_FIRST_TIME_SIGNIN) })
	public User getUserById(Integer userId);

	/**
	 * This method is used to insert Password in password table
	 * 
	 * @param userPassword
	 * @return
	 */
	@Insert(QueryConstant.INSERT_PWRD)
	@Options(useGeneratedKeys = true, keyProperty = "passwordId")
	public int insertPassword(UserPassword userPassword);

	@Select(QueryConstant.SELECT_PWRD_BY_USERID)
	@Results({ @Result(id = true, property = "passwordId", column = "password_id"),
			@Result(property = "userId", column = "user_id"),
			@Result(property = "userPassword", column = "user_password"),
			@Result(property = "timeStamp", column = "time_stamp") })
	public List<UserPassword> getUserPasswordByUserId(int id);

	@Select(QueryConstant.SELECT_USER_BY_EMAIL)
	@Results({ @Result(id = true, property = UserProperties.ID, column = UserColumns.ID),
			@Result(property = UserProperties.FIRSTNAME, column = UserColumns.FIRSTNAME),
			@Result(property = UserProperties.LASTNAME, column = UserColumns.LASTNAME),
			@Result(property = UserProperties.EMAIL, column = UserColumns.EMAIL),
			@Result(property = UserProperties.MOBILE, column = UserColumns.MOBILE),
			@Result(property = UserProperties.CONTINENT, column = UserColumns.CONTINENT),
			@Result(property = UserProperties.COUNTRY, column = UserColumns.COUNTRY),
			@Result(property = UserProperties.STATE, column = UserColumns.STATE),
			@Result(property = UserProperties.CITY, column = UserColumns.CITY),
			@Result(property = UserProperties.ZIPCODE, column = UserColumns.ZIPCODE),
			@Result(property = UserProperties.ORGANIZATION, column = UserColumns.ORGANIZATION),
			@Result(property = UserProperties.IS_FIRST_TIME_SIGNIN, column = UserColumns.IS_FIRST_TIME_SIGNIN),
			@Result(property = "userPassword", javaType = List.class, column = UserColumns.ID, many = @Many(select = "getUserPasswordByUserId")) })
	public User getUserByEmailAddress(String emailId);

	@Update(QueryConstant.UPDATE_USER)
	@Results({ @Result(id = true, property = UserProperties.ID, column = UserColumns.ID),
			@Result(property = UserProperties.FIRSTNAME, column = UserColumns.FIRSTNAME),
			@Result(property = UserProperties.LASTNAME, column = UserColumns.LASTNAME),
			@Result(property = UserProperties.EMAIL, column = UserColumns.EMAIL),
			@Result(property = UserProperties.MOBILE, column = UserColumns.MOBILE),
			@Result(property = UserProperties.CONTINENT, column = UserColumns.CONTINENT),
			@Result(property = UserProperties.COUNTRY, column = UserColumns.COUNTRY),
			@Result(property = UserProperties.STATE, column = UserColumns.STATE),
			@Result(property = UserProperties.CITY, column = UserColumns.CITY),
			@Result(property = UserProperties.ZIPCODE, column = UserColumns.ZIPCODE),
			@Result(property = UserProperties.ORGANIZATION, column = UserColumns.ORGANIZATION),
			@Result(property = UserProperties.IS_FIRST_TIME_SIGNIN, column = UserColumns.IS_FIRST_TIME_SIGNIN) })
	public int updateUser(User user);

	@Delete(QueryConstant.UPDATE_USER_FOR_DELETE)
	public int deleteUser(Integer id);

	@Select(QueryConstant.SELECT_USER_BY_NAME)
	@Results({ @Result(id = true, property = UserProperties.ID, column = UserColumns.ID),
			@Result(property = UserProperties.FIRSTNAME, column = UserColumns.FIRSTNAME),
			@Result(property = UserProperties.LASTNAME, column = UserColumns.LASTNAME),
			@Result(property = UserProperties.EMAIL, column = UserColumns.EMAIL),
			@Result(property = UserProperties.MOBILE, column = UserColumns.MOBILE),
			@Result(property = UserProperties.CONTINENT, column = UserColumns.CONTINENT),
			@Result(property = UserProperties.COUNTRY, column = UserColumns.COUNTRY),
			@Result(property = UserProperties.STATE, column = UserColumns.STATE),
			@Result(property = UserProperties.CITY, column = UserColumns.CITY),
			@Result(property = UserProperties.ZIPCODE, column = UserColumns.ZIPCODE),
			@Result(property = UserProperties.ORGANIZATION, column = UserColumns.ORGANIZATION),
			@Result(property = UserProperties.IS_FIRST_TIME_SIGNIN, column = UserColumns.IS_FIRST_TIME_SIGNIN),
			@Result(property = "contohs", javaType = List.class, column = "nama", many = @Many(select = "getContohs")) })
	public List<User> getUserByName(String userName);

}
