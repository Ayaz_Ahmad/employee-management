package com.hcl.employee.management.serviceimpl;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.employee.management.exception.UserNotUpdatedException;
import com.hcl.employee.management.model.Mail;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserDto;
import com.hcl.employee.management.model.UserPassword;
import com.hcl.employee.management.repository.usermapper.UserMapper;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.AppConstant;
import com.hcl.employee.management.utils.Util;

/**
 * 
 * UserService class is used as a service layer to access business logic
 * implementation of User module.
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	private Logger logger;

	@Autowired
	public UserServiceImpl(Logger logger) {
		this.logger = logger;
		logger.info(AppConstant.USER_SERVICE_BEAN);
	}

	@Autowired
	UserMapper userMapper;
	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private EmailServiceImpl emailService;

	/**
	 * {@inheritDoc}
	 * 
	 * @param emailAddress
	 */
	@Override
	public boolean isUserExistByEmail(String emailAddress) {
		User user = userMapper.getUserByEmailAddress(emailAddress);
		return user != null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 *
	 */
	@Override
	public List<User> getUserByName(String name) {
		return userMapper.getUserByName(name);
	}

	/**
	 * {@inheritDoc}
	 * 
	 *
	 */
	@Override
	public boolean validateSignIn(String emailAddress, String password) {
		return checkUserEmailAndPassword(emailAddress, password);
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public UserDto addUser(User user) {

		user.setFirstTimeSignIn(false);
		userMapper.insertUser(user);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		UserPassword userPassword = new UserPassword();
		userPassword.setUserId(user.getId());
		userPassword.setUserPassword(bcryptEncoder.encode(user.getPassword()));
		userPassword.setTimeStamp(timestamp + "");
		userMapper.insertPassword(userPassword);

		return getUserDto(user);
	}

	/**
	 * {@inheritDoc}
	 * 
	 *
	 */
	@Override
	public Boolean isUserExist(int id) {
		User user = userMapper.getUserById(id);
		return user != null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public User findUserById(int id) {
		return userMapper.getUserById(id);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Override
	public List<User> findAllUsers() {
		return userMapper.findAllUsers();
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public int deleteUserById(int userId) {
		return userMapper.deleteUser(userId);
	}

	/**
	 * This method is used to get user object from UserDto object.
	 * 
	 * @Param User
	 * @return returns UserDto.
	 */
	public UserDto getUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setEmail(user.getEmail());
		userDto.setMobile(user.getMobile());
		userDto.setContinent(user.getContinent());
		userDto.setCountry(user.getCountry());
		userDto.setState(user.getState());
		userDto.setCity(user.getCity());
		userDto.setZipCode(user.getZipCode());
		userDto.setOrganization(user.getOrganization());
		userDto.setPassword(user.getPassword());
		userDto.setFirstTimeSignIn(user.isFirstTimeSignIn());

		return userDto;
	}

	/**
	 * This method is used to get UserDto object from User object.
	 * 
	 * @Param UserDto
	 * @return returns User.
	 */
	public User getUserFromDto(UserDto userDto) {
		User user = new User();
		user.setId(userDto.getId());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		user.setMobile(userDto.getMobile());
		user.setContinent(userDto.getContinent());
		user.setCountry(userDto.getCountry());
		user.setState(userDto.getState());
		user.setCity(userDto.getCity());
		user.setZipCode(userDto.getZipCode());
		user.setOrganization(userDto.getOrganization());
		user.setPassword(userDto.getPassword());
		user.setFirstTimeSignIn(userDto.isFirstTimeSignIn());
		return user;
	}

	/**
	 * {@inheritDoc}
	 * 
	 */

	@Override
	public User updateUser(UserDto userDto) {

		User user = findUserById(userDto.getId());

		if (!StringUtils.isEmpty(userDto.getFirstName())) {

			user.setFirstName(userDto.getFirstName());
		}
		if (!StringUtils.isEmpty(userDto.getLastName())) {
			user.setLastName(userDto.getLastName());
		}
		if (!StringUtils.isEmpty(userDto.getEmail())) {
			user.setEmail(userDto.getEmail());
		}

		if (!StringUtils.isEmpty(userDto.getMobile())) {
			user.setMobile(userDto.getMobile());
		}

		if (!StringUtils.isEmpty(userDto.getContinent())) {
			user.setContinent(userDto.getContinent());
		}

		if (!StringUtils.isEmpty(userDto.getCountry())) {
			user.setCountry(userDto.getCountry());
		}
		if (!StringUtils.isEmpty(userDto.getState())) {
			user.setState(userDto.getState());
		}
		if (!StringUtils.isEmpty(userDto.getCity())) {
			user.setCity(userDto.getCity());
		}
		if (userDto.getZipCode() > 0) {
			user.setZipCode(userDto.getZipCode());
		}

		if (!StringUtils.isEmpty(userDto.getOrganization())) {
			user.setOrganization(userDto.getOrganization());
		}
		if (!StringUtils.isEmpty(userDto.getPassword())) {
			user.setPassword(bcryptEncoder.encode(userDto.getPassword()));
		}

		int updatedRows = userMapper.updateUser(user);
		if (updatedRows < 1) {
			logger.error(AppConstant.USER_NOT_UPDATED);
			throw new UserNotUpdatedException(AppConstant.USER_NOT_UPDATED);
		}

		return findUserById(userDto.getId());
	}

	/**
	 * This method is used to send password on user's email ID.
	 * 
	 */
	@Override
	public boolean sendPasswordOnEmail(String email) {
		User user = getUserByEmail(email);
		boolean isEmailSent = false;
		if (user != null) {
			Util util = new Util();
			String tempPassword = util.getRandomPwd(8);
			user.setPassword(tempPassword);
			updatePassword(user);

			Mail mail = new Mail();
			mail.setPwdUpdated(tempPassword);
			mail.setSubject(AppConstant.EMAIL_SUBJECT);
			mail.setTo(user.getEmail());
			emailService.sendEmail(mail);
			isEmailSent = true;
		}
		return isEmailSent;
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public User getUserByEmail(String email) {
		return userMapper.getUserByEmailAddress(email);
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public boolean checkIsFirstTimeSignIn(String email, String password) {
		User user = getUserByEmail(email);
		if (user != null) {
			return user.isFirstTimeSignIn();
		} else {
			return false;
		}
	}

	/**
	 * This method is used to check if user is valid.
	 * 
	 */
	@Override
	public boolean checkUserEmailAndPassword(String email, String password) {
		User user = getUserByEmail(email);

		if (user != null) {
			List<UserPassword> userPwdList = user.getUserPassword();

			if (bcryptEncoder.matches(password, userPwdList.get(userPwdList.size() - 1).getUserPassword())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public User updatePassword(User user) {
		// Inserting password in password table
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		UserPassword userPassword = new UserPassword();
		userPassword.setUserId(user.getId());
		userPassword.setUserPassword(bcryptEncoder.encode(user.getPassword()));
		userPassword.setTimeStamp(timestamp + "");
		userMapper.insertPassword(userPassword);

		return findUserById(user.getId());
	}

}
