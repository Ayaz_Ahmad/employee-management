package com.hcl.employee.management.serviceimpl;

import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.model.Mail;
import com.hcl.employee.management.service.EmailService;
import com.hcl.employee.management.utils.AppConstant;

/**
 * 
 * Service class used to send email on user's emailId.
 *
 */
@Service
public class EmailServiceImpl implements EmailService {
	private final Logger logger;

	@Autowired
	public EmailServiceImpl(final Logger logger) {
		this.logger = logger;
	}

	@Autowired
	private JavaMailSender emailSender;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean sendEmail(Mail email) {
		boolean result = false;
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());
			helper.setText("I've reset your password is <b>" + email.getPwdUpdated() + "</b>", true);
			helper.setTo(email.getTo());
			helper.setSubject(email.getSubject());
			logger.info(AppConstant.MAIL_SENT);
			emailSender.send(message);
			result = true;
		} catch (MailException | MessagingException e) {
			throw new UserNotFoundException(AppConstant.EXCEPTION_MSG + e.getMessage());
		}
		return result;
	}
}
