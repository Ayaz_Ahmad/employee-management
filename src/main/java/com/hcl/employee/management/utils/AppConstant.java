package com.hcl.employee.management.utils;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

/**
 * This class is used for required literal constant(hard coded value) in project
 * 
 * @author Ayaz Ahmad
 *
 */
public class AppConstant {
	private AppConstant() {
		  
	  }
	public static final String JWT_SECRET = "user_mgt";
	public static final Contact DEFAULT_CONTACT = new Contact("UserManagement API Team", "http://www.hcl.com",
			"usermanagement@hcl.com");

	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("UserManagement API",
			"UserManagement API for cleint implementation.", "1.0", "urn:tos", DEFAULT_CONTACT, "UserManagement 1.0",
			"http://www.hcl.com");

	/* Logging public static final String Constant */
	public static final String FALSE = " = false";
	public static final String EMAIL_DOES_NOT_EXIST = "Email does not exist.";
	public static final String PSRD_DOES_NOT_EXIST = "Password is not correct.";
	public static final String USER_LOGIN_SUCCESS = "User login success.";
	public static final String USER_ID_OR_PSRD_ERROR = "User id or password error.";
	public static final String USER_NEED_TO_CHANGE_PSRD = "User need to change password.";
	public static final String USER_WITH_EMAIL_EXIST = "User with this email already exists.";
	public static final String USER_WITH_EMAIL_NOT_EXIST = "User with this email does not exist.";
	public static final String USER_WITH_PSRD_EXIST = "User with this phone number already exists.";
	public static final String USER_CREATED = "User cuccessfully created.";
	public static final String SUCCESSFULLY_PSRD_SEND = "Password send to your email.";

	public static final String EMAIL_SUBJECT = "User Management :: Forget-Password";
	public static final String USER_NOT_FOUND_IN_DATABASE = "User not found in database.";
	public static final String USERS_NOT_FOUND_IN_DATABASE = "Users not found in database.";
	public static final String USER_FOUND = "User found.";
	public static final String USER_UPDATED_SUCCESSFULLY = "User updated successfully.";
	public static final String USER_PSRD_UPDATED = "User password updated successfully.";
	public static final String USER_PSRD_MUST_BE_SAME = "New password and confirm password must be same.";
	public static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully.";
	public static final String USER_ID_MUST_NOT_BE_EMPTY = "User id must not be empty.";
	public static final String USER_NOT_UPDATED = "User not updated.";
	public static final String EMAIL_MUST_NOT_BE_EMPTY = "Email must not be empty.";
	public static final String USERCONTROLLER_BEAN_INITIALIZED = "UserController bean initialized.";
	public static final String EXCEPTION_MSG = "Exception meassge:";
	public static final String UNABLE_TO_GET_TOKEN = "Unable to get token.";
	public static final String TOKEN_EXPIRED = "Token has expired.";
	public static final String INTERCEPTOR_CALL = "Interceptor doFilterInternal method is called.";

	public static final String MAIL_SENT = "Mail sent.";
	public static final String USER_SERVICE_BEAN = "UserServiceImpl bean initialized.";

}
