package com.hcl.employee.management.utils;

public class UserProperties {
	private UserProperties() {
// To block instance creation
	}

	public static final String MOBILE = "mobile";
	public static final String COUNTRY = "country";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipCode";
	public static final String EMAIL = "email";
	public static final String CITY = "city";
	public static final String ORGANIZATION = "organization";
	public static final String CONTINENT = "continent";
	public static final String PSRD = "password";
	public static final String IS_FIRST_TIME_SIGNIN = "isFirstTimeSignIn";
	public static final String FIRSTNAME = "firstName";
	public static final String IS_DELETED = "isDeleted";
	public static final String ID = "id";
	public static final String LASTNAME = "lastName";
}