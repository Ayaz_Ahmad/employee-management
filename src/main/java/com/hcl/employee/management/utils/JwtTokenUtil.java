package com.hcl.employee.management.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * The class is responsible for performing JWT operations like creation and
 * validation. It makes use of the io.jsonwebtoken.Jwts for achieving this.
 *
 */
@Component
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = -2550185165626007488L;

	public static final long JWT_TOKEN_VALIDITY = 10 * 60 * 60 * 1000L; // 10 hours

	/**
	 * retrieve Email from jwt token
	 * 
	 * @param token
	 * @return email
	 */
	public String getEmailFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	/**
	 * retrieve Id from jwt token
	 * 
	 * @param token
	 * @return id
	 */
	public String getIDFromToken(String token) {
		return getClaimFromToken(token, Claims::getId);
	}

	/**
	 * retrieve expiration date from jwt token
	 * 
	 * @param token
	 * @return expiration date
	 */
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	// for retrieving any information from token we will need the secret key
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(AppConstant.JWT_SECRET).parseClaimsJws(token).getBody();
	}

	/**
	 * check if the token has expired
	 * 
	 * @param token
	 * @return true if token is expired otherwise false
	 */
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	/**
	 * generate token for user
	 * 
	 * @param email
	 * @return generated token
	 */
	public Object generateToken(String email, int id) {
		Map<String, Object> map = new HashMap<>();
		return doGenerateToken(map, email, id);
	}

	/**
	 * while creating the token - 1. Define claims of the token, like Issuer,
	 * Expiration, Subject, and the ID 2. Sign the JWT using the HS512 algorithm and
	 * secret key. 3. According to JWS Compact
	 * Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
	 * compaction of the JWT to a URL-safe string
	 * 
	 * @param claims
	 * @param subject
	 * @return generated token
	 */
	private String doGenerateToken(Map<String, Object> claims, String subject, int id) {

		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setId(id + "").setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
				.signWith(SignatureAlgorithm.HS512, AppConstant.JWT_SECRET).compact();
	}

	/**
	 * validate token
	 * 
	 * @param token
	 * @param email
	 * @return true if token is valid otherwise false
	 */
	public boolean validateToken(String token) {
		return !isTokenExpired(token);
	}
}