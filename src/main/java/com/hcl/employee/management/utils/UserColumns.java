package com.hcl.employee.management.utils;

public class UserColumns {
	private UserColumns() {
		// To block instance creation
	}

	public static final String CITY = "city";
	public static final String ID = "user_id";
	public static final String MOBILE = "mobile";
	public static final String ORGANIZATION = "organization";
	public static final String FIRSTNAME = "first_name";
	public static final String IS_FIRST_TIME_SIGNIN = "isFirstTimeSignIn";
	public static final String LASTNAME = "last_name";
	public static final String EMAIL = "email_id";
	public static final String IS_DELETED = "is_deleted";
	public static final String CONTINENT = "continent";
	public static final String COUNTRY = "country";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipCode";
	public static final String PSRD = "password";
}