package com.hcl.employee.management.utils;

/**
 * This class is used for query constant(hard coded value) in project
 * 
 */
public class QueryConstant {
	private QueryConstant() {

	}

	public static final String SELECT_USER_BY_ID = "SELECT * FROM USER WHERE USER_ID = #{userId} and "
			+ UserColumns.IS_DELETED + AppConstant.FALSE;

	public static final String SELECT_USER_BY_EMAIL = "SELECT * FROM USER " + "WHERE EMAIL_ID = #{emailId} and "
			+ UserColumns.IS_DELETED + AppConstant.FALSE;
	public static final String SELECT_PWRD_BY_USERID = "SELECT * FROM USER_PASSWORD " + "WHERE USER_ID = #{user_id}";

	public static final String INSERT_PWRD = "INSERT INTO" + " USER_PASSWORD(user_id," + "	user_password, time_stamp)"
			+ "	VALUES(#{userId}, #{userPassword}," + "	#{timeStamp})";

	public static final String SELECT_USER_BY_NAME = "SELECT * FROM USER WHERE " + UserColumns.FIRSTNAME + " LIKE "
			+ "'%'||#{userName}||'%' and " + UserColumns.IS_DELETED + AppConstant.FALSE;

	public static final String INSERT_INTO_USER = "INSERT INTO" + " USER(first_name,last_name,"
			+ "email_id,mobile,continent,country,state,city,zipCode,organization,isFirstTimeSignIn) "
			+ "VALUES(#{firstName}, #{lastName}, #{email}, #{mobile}, #{continent},"
			+ "#{country}, #{state}, #{city}, #{zipCode}, #{organization}," + "#{isFirstTimeSignIn})";

	public static final String UPDATE_USER = "UPDATE USER SET FIRST_NAME = #{firstName},"
			+ "	LAST_NAME = #{lastName}, EMAIL_ID = #{email}, mobile = #{mobile}," + "	continent = #{continent},"
			+ "	country = #{country}, state = #{state}, city = #{city}, zipCode = #{zipCode},"
			+ "	organization = #{organization},	isFirstTimeSignIn = #{isFirstTimeSignIn}" + "	WHERE"
			+ "	USER_ID = #{id}";

	public static final String UPDATE_USER_FOR_DELETE = "UPDATE USER SET " + UserColumns.IS_DELETED + "=true"
			+ "	WHERE" + "	USER_ID = #{id}";

	public static final String SELECT_ALL_FROM_USER = "SELECT * FROM USER where " + UserColumns.IS_DELETED
			+ AppConstant.FALSE;

}
