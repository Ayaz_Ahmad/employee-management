package com.hcl.employee.management.utils;

import java.security.SecureRandom;

public class Util {
	private SecureRandom  rand = new SecureRandom();

	public String getRandomPwd(int n) {

		// chose a Character random from this String
		String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz"
				+ "!@#$%^&*()_-+=|}]{['<>.,;?";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {
			// generate a random number between
			// 0 to AlphaNumericString variable length

			int value = rand.nextInt(alphaNumericString.length() - 1);
			// add Character one by one in end of sb
			sb.append(alphaNumericString.charAt(value));
		}
		return sb.toString();
	}
}
