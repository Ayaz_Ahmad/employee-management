package com.hcl.employee.management.configuration;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.hcl.employee.management.repository.dbmapper.MultipleMapper;
import com.hcl.employee.management.repository.usermapper.UserMapper;

/**
 * 
 * This class is for MyBatis configuration for multiple data source beam
 * registration.
 * 
 * @MapperScan is used to refer other SessionFactory name to create the mapper.
 *
 */
@Configuration
@MapperScan(value = {
		"com.hcl.employee.management.repository.dbmapper" }, sqlSessionFactoryRef = "anotherSessionFactory")
public class MyBatisConfiguration {
	private static final String ONE_SESSION_FACTORY = "oneSessionFactory";
	private static final String ANOTHER_SESSION_FACTORY = "anotherSessionFactory";

	/**
	 * DataSource one with qualifier name PRIMARY_DATASOURCE.
	 */
	@Autowired
	@Qualifier(DatabaseConfiguration.PRIMARY_DATASOURCE)
	DataSource mOneDataSource;

	/**
	 * DataSource two with qualifier name SECONDARY_DATASOURCE .
	 */
	@Autowired
	@Qualifier(DatabaseConfiguration.SECONDARY_DATASOURCE)
	DataSource mOtherDataSource;

	/**
	 * This is primary SqlSessionFactory bean to register the mapper associated with
	 * DataSource1.
	 * 
	 * @param oneDataSource
	 * @return
	 * @throws Exception
	 */
	@Bean(name = ONE_SESSION_FACTORY, destroyMethod = "")
	@Primary
	public SqlSessionFactoryBean sqlSessionFactoryOne(
			@Qualifier(DatabaseConfiguration.PRIMARY_DATASOURCE) final DataSource oneDataSource) throws Exception {
		final SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(oneDataSource);
		SqlSessionFactory sqlSessionFactory;
		sqlSessionFactory = sqlSessionFactoryBean.getObject();
		sqlSessionFactory.getConfiguration().addMapper(UserMapper.class);
		// Various other SqlSessionFactory settings
		return sqlSessionFactoryBean;
	}

	@Bean
	@Primary
	public MapperFactoryBean<UserMapper> userMapper(
			@Qualifier(ONE_SESSION_FACTORY) final SqlSessionFactoryBean sqlSessionFactoryBean) throws Exception {
		MapperFactoryBean<UserMapper> factoryBean = new MapperFactoryBean<>(UserMapper.class);
		factoryBean.setSqlSessionFactory(sqlSessionFactoryBean.getObject());
		return factoryBean;
	}

	@Bean
	public MapperFactoryBean<MultipleMapper> userMapperAnnotation(
			@Qualifier(ONE_SESSION_FACTORY) final SqlSessionFactoryBean sqlSessionFactoryBean) throws Exception {
		MapperFactoryBean<MultipleMapper> factoryBean = new MapperFactoryBean<>(MultipleMapper.class);
		factoryBean.setSqlSessionFactory(sqlSessionFactoryBean.getObject());
		return factoryBean;
	}

	@Bean(name = ANOTHER_SESSION_FACTORY, destroyMethod = "")
	public SqlSessionFactoryBean sqlSessionFactoryTwo(
			@Qualifier(DatabaseConfiguration.SECONDARY_DATASOURCE) final DataSource otherDataSource) throws Exception {
		final SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(otherDataSource);
		SqlSessionFactory sqlSessionFactory;
		sqlSessionFactory = sqlSessionFactoryBean.getObject();
		sqlSessionFactory.getConfiguration().addMapper(MultipleMapper.class);
		// Various other SqlSessionFactory settings
		return sqlSessionFactoryBean;
	}

	@Bean
	public MapperFactoryBean<MultipleMapper> etrMultipleMapper(
			@Qualifier(ANOTHER_SESSION_FACTORY) final SqlSessionFactoryBean sqlSessionFactoryBean) throws Exception {
		MapperFactoryBean<MultipleMapper> factoryBean = new MapperFactoryBean<>(MultipleMapper.class);
		factoryBean.setSqlSessionFactory(sqlSessionFactoryBean.getObject());
		return factoryBean;
	}

}
