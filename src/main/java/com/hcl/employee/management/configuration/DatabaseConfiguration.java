package com.hcl.employee.management.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfiguration {

	public static final String PRIMARY_DATASOURCE = "OneDS";
	public static final String SECONDARY_DATASOURCE = "AnotherDS";

	@Bean(name = PRIMARY_DATASOURCE, destroyMethod = "")
	@Primary
	public DataSource dataSourceOne() {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/passwordbasedschema");
		dataSource.setUsername("root");
		dataSource.setPassword("root");

		return dataSource;
	}

	@Bean(name = SECONDARY_DATASOURCE, destroyMethod = "")
	public DataSource dataSourceAnother() {
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/testschema");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}
	


}
