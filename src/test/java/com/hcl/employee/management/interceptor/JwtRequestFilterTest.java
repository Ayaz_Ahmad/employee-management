package com.hcl.employee.management.interceptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.hcl.employee.management.controller.base.BaseTestClass;
import com.hcl.employee.management.model.UserPassword;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.JwtTokenUtil;

class JwtRequestFilterTest extends BaseTestClass {

	private MockMvc mockMvc;

	@Mock
	UserService userService;
	
	@Mock
	JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private WebApplicationContext context;

	@Mock
	HttpServletResponse httpServletResponse;
	
	@Mock
	FilterChain filterChain;

	@InjectMocks
	JwtRequestFilter jwtRequestFilter;

	@BeforeEach
	public void setup() {
		// this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).addFilter(jwtRequestFilter, "/*").build();
	}

	@Test
	public void doFilterInternalTest() throws Exception {

		when(userService.isUserExistByEmail(user.getEmail())).thenReturn(true);
		when(jwtTokenUtil.getEmailFromToken(token)).thenReturn(user.getEmail());
		when(userService.getUserByEmail(user.getEmail())).thenReturn(user);
		when(jwtTokenUtil.validateToken(token)).thenReturn(true);
		
		

		UserPassword userPassword = new UserPassword();
		userPassword.setUserId(user.getId());
		userPassword.setUserPassword("encoded_password");

		String password = "dummypassword";
		ArrayList<UserPassword> userPasswordList = new ArrayList<UserPassword>();

		userPassword.setUserPassword(password);
		userPasswordList.add(userPassword);
		user.setUserPassword(userPasswordList);
		
		MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
		httpServletRequest.addHeader("authorizationtoken", token);

		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
				user.getUserPassword().get(user.getUserPassword().size() - 1).getUserPassword(), new ArrayList<>());
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				userDetails, null, userDetails.getAuthorities());
		usernamePasswordAuthenticationToken
				.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

		jwtRequestFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);


		verify(userService, times(1)).getUserByEmail(user.getEmail());


	}

}
