package com.hcl.employee.management.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.AutoConfigureMybatis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hcl.employee.management.EmployeeManagementApplication;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserPassword;
import com.hcl.employee.management.repository.usermapper.UserMapper;

@AutoConfigureMybatis
@Transactional
@SpringBootTest(classes = EmployeeManagementApplication.class)
class UserMapperTest {

	private User user;
	private UserPassword userPassword;

	@BeforeEach
	public void setUp() throws Exception {

		user = new User();
		user.setEmail("te21@test.com");
		user.setFirstName("TestFirstName");
		user.setLastName("TestLastName");
		user.setMobile("1234567898");
		user.setContinent("Asia");
		user.setCountry("India");
		user.setState("Uttar Pradesh");
		user.setCity("Lucknow");
		user.setZipCode(226001);
		user.setOrganization("TACT");
		user.setPassword("12345");
		user.setFirstTimeSignIn(false);

		userPassword = new UserPassword();
		userPassword.setTimeStamp("" + System.currentTimeMillis());
		userPassword.setUserPassword("testPassword");

	}

	@Autowired
	private UserMapper userMapper;

	@Test
	public void testInsertUser() {

		int addedRows = userMapper.insertUser(user);
		assertEquals(1, addedRows);
	}

	@Test
	public void testInsertPassword() {

		userMapper.insertUser(user);
		userPassword.setUserId(user.getId());
		int addedRows = userMapper.insertPassword(userPassword);
		assertEquals(1, addedRows);
	}

	@Test
	public void testGetUserPasswordByUserId() {

		userMapper.insertUser(user);
		userPassword.setUserId(user.getId());
		userMapper.insertPassword(userPassword);

		List<UserPassword> passwordList = userMapper.getUserPasswordByUserId(user.getId());
		assertEquals(1, passwordList.size());
	}

	@Test
	public void testGetUserById() {
		userMapper.insertUser(user);
		User userResult = userMapper.getUserById(user.getId());
		assertNotNull(userResult);
	}

	@Test
	public void testFindAllUsers() {
		List<User> user = userMapper.findAllUsers();
		assertNotNull(user);
	}

	@Test
	public void testUpdateUser() {
		userMapper.insertUser(user);
		user.setFirstName("TestFirstName66 updated");
		user.setLastName("TestLastName66 updated");
		int updatedRows = userMapper.updateUser(user);
		assertEquals(1, updatedRows);
	}

	@Test
	public void testDeleteUser() {
		userMapper.insertUser(user);
		int deleted = userMapper.deleteUser(user.getId());
		assertEquals(1, deleted);
	}

	@Test
	public void testGetUserByEmailAddress() {
		userMapper.insertUser(user);
		User userResult = userMapper.getUserByEmailAddress(user.getEmail());
		assertNotNull(userResult);
	}

	@Test
	public void testGetUserByName() {
		userMapper.insertUser(user);
		List<User> users = userMapper.getUserByName(user.getFirstName());
		assertNotNull(users);
	}

}