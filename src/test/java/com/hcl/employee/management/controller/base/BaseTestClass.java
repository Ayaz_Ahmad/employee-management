package com.hcl.employee.management.controller.base;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.employee.management.EmployeeManagementApplication;
import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserDto;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.JwtTokenUtil;

//@AutoConfigureMybatis
@Transactional
@SpringBootTest(classes = EmployeeManagementApplication.class)
public class BaseTestClass {

	protected MockMvc mvc;
	@Autowired
	private UserService iUserServiceReal;
	@Autowired
	private JwtTokenUtil jwtTokenUtilReal;

	@Autowired
	private WebApplicationContext webApplicationContext;

	protected User user;
	protected UserDto userDto;
	protected String token;
	protected int insertedUserId;

	@BeforeEach
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
		user = new User();
		user.setFirstName("Ayaz");
		user.setLastName("Ahmad");
		user.setPassword("Ayaz@123");

		user.setMobile("9695779067");
		user.setEmail("dummy@gmail.com");

		userDto = new UserDto();
		userDto.setFirstName("Ayaz");
		userDto.setLastName("Ahmad");
		userDto.setEmail("dummy@gmail.com");
		userDto.setMobile("9695779067");
		userDto.setContinent("Asia");
		userDto.setCountry("India");
		userDto.setState("Uttar Pradesh");
		userDto.setCity("Lucknow");
		userDto.setZipCode(226001);
		userDto.setOrganization("TACT");
		userDto.setPassword("Ayaz@123");
		userDto.setFirstTimeSignIn(true);

		UserDto insertedUser = iUserServiceReal.addUser(user);
		insertedUserId = insertedUser.getId();
		token = jwtTokenUtilReal.generateToken(insertedUser.getEmail(), insertedUser.getId()).toString();

	}

	protected void setUserDtoId(int id) {
		userDto.setId(id);
	}
	
	protected String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}

	protected int getStatusValue(String inputJson, String uri, String token) throws UserNotFoundException, Exception {
		MvcResult mvcResult = null;

		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).header("authorizationtoken", token)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		return status;
	}

	protected int getStatusValue(String inputJson, String uri) throws UserNotFoundException, Exception {
		MvcResult mvcResult = null;

		mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		return status;
	}

	/**
	 * Common method for all above method this method result mockmvcResult.
	 */
	protected int getStatusValueGET(String uri, String token) throws Exception{
		MvcResult mvcResult = null;
		mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).header("authorizationtoken", token)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		return status;
	}

	protected int getStatusValueGET(String uri) throws Exception{
		MvcResult mvcResult = null;
		mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		return status;
	}
}
