package com.hcl.employee.management.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.employee.management.controller.base.BaseTestClass;
import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.service.UserService;
import com.hcl.employee.management.utils.JwtTokenUtil;

class UserControllerTest extends BaseTestClass {

	@Mock
	private UserService iUserService;

	@Mock
	private JwtTokenUtil jwtTokenUtil;
	@Mock
	private Logger logger;

	@InjectMocks
	private UserController userController;

	@Test
	public void forgetPasswordTestWithFail() throws Exception {
		String uri = "/forgetPassword";
		user.setEmail("tempemail@hcl.com");
		when(iUserService.isUserExistByEmail(user.getEmail())).thenReturn(false);
		String inputJson = mapToJson(user);
		int status = getStatusValue(inputJson, uri);
		assertEquals(400, status);
	}

	@Test
	public void forgetPasswordTestWithSuccess() throws Exception {
		String uri = "/forgetPassword";
		when(iUserService.isUserExistByEmail(user.getEmail())).thenReturn(true);
		when(iUserService.sendPasswordOnEmail(user.getEmail())).thenReturn(true);
		String inputJsonForrgotPass = mapToJson(user);
		int status = getStatusValue(inputJsonForrgotPass, uri, "");
		assertEquals(200, status);
	}

	@Test
	public void userSignupTestWithSuccess() throws Exception {
		String uri = "/usersignup";
		userDto.setEmail("dummyemail@hcl.com");
		when(iUserService.getUserFromDto(userDto)).thenReturn(user);
		when(iUserService.isUserExistByEmail(user.getEmail())).thenReturn(false);
		when(iUserService.addUser(user)).thenReturn(userDto);
		when(jwtTokenUtil.generateToken(user.getEmail(), user.getId())).thenReturn("any_token_string");
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(201, status);
	}

	@Test
	public void userSignupTestWithFail() throws Exception {
		String uri = "/usersignup";
		userDto.setEmail(userDto.getEmail());
		when(iUserService.getUserFromDto(userDto)).thenReturn(user);
		when(iUserService.isUserExistByEmail(userDto.getEmail())).thenReturn(true);
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(400, status);

	}

	@Test
	public void userSignInTestWithSuccess() throws Exception {
		String uri = "/usersignin";
		when(iUserService.isUserExistByEmail(user.getEmail())).thenReturn(true);
		when(iUserService.checkIsFirstTimeSignIn(user.getEmail(), user.getPassword())).thenReturn(false);
		when(iUserService.validateSignIn(user.getEmail(), user.getPassword())).thenReturn(true);
		when(iUserService.getUserByEmail(user.getEmail())).thenReturn(new User());
		when(jwtTokenUtil.generateToken(user.getEmail(), user.getId())).thenReturn("any_token_string");
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(200, status);
	}

	@Test
	public void userSignInTestWithFailFirst() throws Exception {
		String uri = "/usersignin";
		user.setEmail("indbdoesnotexist@gmail.com");
		when(iUserService.isUserExistByEmail(user.getEmail())).thenReturn(false);
		String inputJson = mapToJson(user);
		int status = getStatusValue(inputJson, uri);
		assertEquals(400, status);
	}

	@Test
	public void changePasswordTestWithSuccess() throws Exception {
		String uri = "/changePassword";

		userDto.setId(insertedUserId);
		userDto.setConfirmPassword(userDto.getPassword());

		when(iUserService.getUserByEmail(userDto.getEmail())).thenReturn(user);
		when(iUserService.updatePassword(user)).thenReturn(user);

		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);

		assertEquals(200, status);

	}

	@Test
	public void changePasswordTestWithDifferentConfirmPassword() throws Exception {

		userDto.setConfirmPassword("mismatched" + userDto.getPassword());
		assertThrows(UserNotFoundException.class, () -> userController.changePassword(userDto));

	}

	@Test
	public void changePasswordTestWithFailException() throws Exception {

		userDto.setConfirmPassword(userDto.getPassword());
		when(iUserService.getUserByEmail(userDto.getEmail())).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> userController.changePassword(userDto));

	}

	@Test
	public void multipleDatasourceDemoTestWithFailEmptyEmailException() throws Exception {

		assertThrows(UserNotFoundException.class, () -> userController.multipleDatasourceDemo(""));

	}

	@Test
	public void multipleDatasourceDemoTestWithFailException() throws Exception {

		when(iUserService.getUserByEmail(userDto.getEmail())).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> userController.multipleDatasourceDemo("dummy@gmail.com"));

	}

	@Test
	public void multipleDatasourceDemoTestWithSuccess() throws Exception {

		when(iUserService.getUserByEmail(userDto.getEmail())).thenReturn(user);
		assertEquals(HttpStatus.OK, userController.multipleDatasourceDemo(userDto.getEmail()).getStatusCode());
	}

	@Test
	public void getUserDetailsWithRestTamplateSuccess() throws Exception {
		String uri = "/updateUser";
		setUserDtoId(0);
		when(iUserService.isUserExist(userDto.getId())).thenReturn(false);
		when(iUserService.updateUser(userDto)).thenReturn(user);
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(404, status);

	}

	@Test
	public void updateUserDetailsWithSuccess() throws Exception {
		String uri = "/updateUser";
		setUserDtoId(insertedUserId);
		when(iUserService.isUserExist(userDto.getId())).thenReturn(true);
		when(iUserService.updateUser(userDto)).thenReturn(user);
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(200, status);

	}

	@Test
	public void updateUserDetailsWithFail() throws Exception {
		String uri = "/updateUser";
		setUserDtoId(-1);
		when(iUserService.isUserExist(userDto.getId())).thenReturn(false);
		when(iUserService.updateUser(userDto)).thenReturn(user);
		String inputJson = mapToJson(userDto);
		int status = getStatusValue(inputJson, uri);
		assertEquals(404, status);
	}

	@Test
	public void getUserDetailsByNameWithSuccess() throws Exception {
		String uri = "/getUserByName/Shashank";
		ArrayList<User> listdata = new ArrayList<User>();
		when(iUserService.getUserByName("Shashank")).thenReturn(listdata);
		int status = getStatusValueGET(uri);
		assertEquals(200, status);
	}

	@Test
	public void getUserDetailsByNameWithFail() throws Exception {
		when(iUserService.getUserByName("Shashank")).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> userController.getUserDetailsByName("Shashank"));
	}

	@Test
	public void getAllUsersWithSuccess() throws Exception {
		String uri = "/getAllUsers";
		ArrayList<User> userList = new ArrayList<User>();
		when(iUserService.findAllUsers()).thenReturn(userList);
		int status = getStatusValueGET(uri);
		assertEquals(200, status);
	}

	@Test
	public void getAllUserWithFail() throws Exception {
		when(iUserService.findAllUsers()).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> userController.getAllUsers());
	}

	@Test
	public void deleteUserDetailsWithFail() throws Exception {
		when(iUserService.isUserExist(-1)).thenReturn(false);
		assertThrows(UserNotFoundException.class, () -> userController.deleteUserDetails(-1));
	}

	@Test
	@Transactional
	public void deleteUserDetailsWithSuccess() throws Exception {

		String uri = "/deleteUser/" + insertedUserId;

		when(iUserService.isUserExist(insertedUserId)).thenReturn(true);
		when(iUserService.deleteUserById(insertedUserId)).thenReturn(1);
		int status = getStatusValueGET(uri, token);
		assertEquals(200, status);
	}

	@Test
	public void getUserDetailsWithSuccess() throws Exception {
		when(jwtTokenUtil.getIDFromToken(token)).thenReturn("1");
		when(iUserService.findUserById(insertedUserId)).thenReturn(user);
		when(iUserService.getUserDto(user)).thenReturn(userDto);
		MvcResult mvcResult = mvc.perform(get("/getUserDetails").contentType(MediaType.APPLICATION_JSON_VALUE)
				.header("authorizationtoken", token)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void getUserDetailsWithFail() throws Exception {
		when(jwtTokenUtil.getIDFromToken(token)).thenReturn("-1");
		when(iUserService.findUserById(insertedUserId)).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> userController.getUserDetails(token));
	}

	@Test
	public void getUserDetailsWithRestTamplateWithFail() throws Exception {

		MvcResult mvcResult = mvc.perform(get("/restTemplate/getUserDetails")
				.contentType(MediaType.APPLICATION_JSON_VALUE).header("authorizationtoken", token)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(500, status);

	}

}