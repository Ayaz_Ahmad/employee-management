package com.hcl.employee.management.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.boot.test.autoconfigure.AutoConfigureMybatis;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.hcl.employee.management.EmployeeManagementApplication;
import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.exception.UserNotUpdatedException;
import com.hcl.employee.management.model.Mail;
import com.hcl.employee.management.model.User;
import com.hcl.employee.management.model.UserDto;
import com.hcl.employee.management.model.UserPassword;
import com.hcl.employee.management.repository.usermapper.UserMapper;
import com.hcl.employee.management.serviceimpl.EmailServiceImpl;
import com.hcl.employee.management.serviceimpl.UserServiceImpl;
import com.hcl.employee.management.utils.Util;

@AutoConfigureMybatis
@SpringBootTest(classes = EmployeeManagementApplication.class)
class UserServiceImplTest {

	@Mock
	private PasswordEncoder bcryptEncoder;
	@Mock
	private Logger logger;
	@Mock
	private UserMapper userMapper;

	@InjectMocks
	private UserServiceImpl service;

	@Mock
	private Util util;

	@Mock
	private EmailServiceImpl emailService;

	private UserDto userDto;
	private User user;
	private UserPassword userPassword;

	@BeforeEach
	public void setUp() throws Exception {
		userDto = new UserDto();
		String email = "Ajeet" + System.currentTimeMillis() + "@gmail.com";
		userDto.setEmail(email);

		userDto.setFirstName("Ajeet");
		userDto.setLastName("AjeetTestLastName");
		userDto.setMobile("1234567898");
		userDto.setContinent("Asia");
		userDto.setCountry("India");
		userDto.setState("Uttar Pradesh");
		userDto.setCity("Lucknow");
		userDto.setZipCode(226001);
		userDto.setOrganization("TACT");
		userDto.setPassword("12345");
		userDto.setFirstTimeSignIn(false);

		user = new User();
		user.setId(59);
		user.setEmail(email);
		user.setFirstName("TestFirstName");
		user.setLastName("TestLastName");
		user.setMobile("1234567898");
		user.setContinent("Asia");
		user.setCountry("India");
		user.setState("Uttar Pradesh");
		user.setCity("Lucknow");
		user.setZipCode(226001);
		user.setOrganization("TACT");
		user.setPassword("12345");
		user.setFirstTimeSignIn(false);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		userPassword = new UserPassword();
		userPassword.setUserId(user.getId());
		userPassword.setUserPassword("encoded_password");
		userPassword.setTimeStamp(timestamp + "");

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAddUserSuccess() {
		when(userMapper.insertUser(user)).thenReturn(1);
		when(bcryptEncoder.encode(user.getPassword())).thenReturn("encoded password");
		when(userMapper.insertPassword(userPassword)).thenReturn(1);
		UserDto dto = service.addUser(user);
		assertNotNull(dto);
		verify(userMapper, times(1)).insertUser(user);

	}

	@Test
	public void testIsUserExistByEmailSuccess() {
		String emailId = user.getEmail();
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(user);
		assertTrue(service.isUserExistByEmail(emailId));
		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
	}

	@Test
	public void testIsUserExistByEmailFail() {
		String emailId = "dummyEmailID@hcl.com";
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(null);
		assertFalse(service.isUserExistByEmail(emailId));
		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
	}

	@Test
	public void testIsUserExistByIdSuccess() {
		int userId = 5;
		when(userMapper.getUserById(userId)).thenReturn(new User());
		assertTrue(service.isUserExist(userId));
		verify(userMapper, times(1)).getUserById(userId);

	}

	@Test
	public void testIsUserExistByIdFail() {
		int userId = 5;
		when(userMapper.getUserById(userId)).thenReturn(null);
		assertFalse(service.isUserExist(userId));
		verify(userMapper, times(1)).getUserById(userId);

	}

	@Test
	public void testFindUserByIdSuccess() {

		int id = 4;
		when(userMapper.getUserById(id)).thenReturn(user);
		assertNotNull(service.findUserById(id));
		verify(userMapper, times(1)).getUserById(id);
	}

	@Test
	public void testFindUserByIdFail() {
		int id = 4;
		when(userMapper.getUserById(id)).thenReturn(null);
		assertNull(service.findUserById(id));
		verify(userMapper, times(1)).getUserById(id);
	}

	@Test
	public void testFindAllUsersSuccess() {

		when(userMapper.findAllUsers()).thenReturn(new ArrayList<>());

		assertNotNull(service.findAllUsers());
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).findAllUsers();
		// verify that the exists method is invoked one time
	}

	@Test
	public void testFindAllUsersFail() {

		when(userMapper.findAllUsers()).thenReturn(null);

		assertNull(service.findAllUsers());
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).findAllUsers();
		// verify that the exists method is invoked one time
	}

	@Test
	public void testGetUserByNameSuccess() {

		String name = "dummyName";
		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByName(name)).thenReturn(new ArrayList<User>());

		assertNotNull(service.getUserByName(name));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByName(name);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testGetUserByNameFail() {

		String name = "dummyName";
		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByName(name)).thenReturn(null);

		assertNull(service.getUserByName(name));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByName(name);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testCheckIsFirstTimeSignInSuccess() {

		String emailId = "dummyEmailId@dummy.com";

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(user);

		assertTrue(service.checkIsFirstTimeSignIn(emailId, null));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testCheckIsFirstTimeSignInFail() {

		String emailId = "dummyEmailId@dummy.com";

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(null);

		assertFalse(service.checkIsFirstTimeSignIn(emailId, null));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testValidateSignInSuccess() {

		String password = "dummypassword";
		String emailId = "dummyEmailId@dummy.com";

		ArrayList<UserPassword> userPasswordList = new ArrayList<UserPassword>();

		userPassword.setUserPassword(password);
		userPasswordList.add(userPassword);
		user.setUserPassword(userPasswordList);

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(user);
		when(bcryptEncoder.matches(password, password)).thenReturn(true);

		assertTrue(service.validateSignIn(emailId, password));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testValidateSignInFail() {

		String password = "dummypassword";
		String emailId = "dummyEmailId@dummy.com";

		ArrayList<UserPassword> userPasswordList = new ArrayList<UserPassword>();

		userPassword.setUserPassword(password);
		userPasswordList.add(userPassword);
		user.setUserPassword(userPasswordList);

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(null);
		when(bcryptEncoder.matches(password, password)).thenReturn(true);

		assertFalse(service.validateSignIn(emailId, password));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).getUserByEmailAddress(emailId);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testDeleteUserById() {
		int id = 4;
		when(userMapper.deleteUser(id)).thenReturn(1);

		assertEquals(1, service.deleteUserById(id));
		// verify that the isUserExistByEmail method has been invoked

		verify(userMapper, times(1)).deleteUser(id);
		// verify that the exists method is invoked one time

	}

	@Test
	public void testDeleteUserByIdFail() {
		int id = -1;
		doThrow(new UserNotFoundException("User not found test: ") {
		}).when(userMapper).deleteUser(id);
		assertThrows(UserNotFoundException.class, () -> service.deleteUserById(id));
		verify(userMapper, times(1)).deleteUser(id);
		// verify that the exists method is invoked one time

	}

	@Test
	public void testGetUserDto() {
		user.setEmail("Email");
		assertEquals("Email", service.getUserDto(user).getEmail());
	}

	@Test
	public void testUpdateUser() {

		userDto.setId(user.getId());
		userDto.setFirstName("Ajeet Updated");
		userDto.setLastName("AjeetTestLastName");

		when(bcryptEncoder.encode("password")).thenReturn("encoded password");
		when(userMapper.getUserById(user.getId())).thenReturn(user);
		when(userMapper.updateUser(user)).thenReturn(1);

		assertNotNull(service.updateUser(userDto));
		verify(userMapper, times(1)).updateUser(user);
		// verify that the exists method is invoked one time
	}

	@Test
	public void testUpdateUserFail() throws UserNotUpdatedException {

		userDto.setId(user.getId());
		userDto.setFirstName("Ajeet Updated");
		userDto.setLastName("AjeetTestLastName");

		when(bcryptEncoder.encode("password")).thenReturn("encoded password");
		when(userMapper.getUserById(user.getId())).thenReturn(user);
		when(userMapper.updateUser(user)).thenReturn(0);
		assertThrows(UserNotUpdatedException.class, () -> service.updateUser(userDto));
		verify(userMapper, times(1)).updateUser(user);
		// verify that the exists method is invoked one time
	}

	@Test
	public void sendPasswordOnEmailWithSuccess() throws Exception {
		String emailId = user.getEmail();
		when(service.getUserByEmail(emailId)).thenReturn(user);
		when(bcryptEncoder.encode("435703948fdfg")).thenReturn("encoded password");
		when(service.updatePassword(user)).thenReturn(user);

		Mail mail = new Mail();
		mail.setPwdUpdated("ayush099");
		mail.setSubject("User Management :: Forget-Password");
		mail.setTo(user.getEmail());
		when(emailService.sendEmail(mail)).thenReturn(true);
		boolean emailExist = service.sendPasswordOnEmail(emailId);
		assertEquals(true, emailExist);
	}

	@Test
	public void sendPasswordOnEmailWithFail() {
		String emailId = "dummyEmailId@hcl.com";
		when(service.getUserByEmail(emailId)).thenReturn(null);
		boolean emailExist = service.sendPasswordOnEmail(emailId);
		assertEquals(false, emailExist);
	}

	@Test
	public void getUserByEmailWithSuccess() {
		String emailId = "dummyEmailId@hcl.com";
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(new User());
		User user = service.getUserByEmail(emailId);
		assertNotNull(user);
	}

	@Test
	public void getUserByEmailWithFail() {
		String emailId = "dummyEmailId@hcl.com";
		when(userMapper.getUserByEmailAddress(emailId)).thenReturn(null);
		User user = service.getUserByEmail(emailId);
		assertNull(user);
	}

	@Test
	public void checkIsFirstTimeSignInWithSuccess() {
		String emailId = "dummyEmailId@hcl.com";
		user.setId(59);
		user.setEmail(emailId);
		user.setPassword("12345");
		user.setFirstTimeSignIn(true);
		when(service.getUserByEmail(emailId)).thenReturn(user);
		boolean firstTimeSignIn = service.checkIsFirstTimeSignIn(emailId, "39475");
		assertEquals(true, firstTimeSignIn);
	}

	@Test
	public void checkIsFirstTimeSignInWithFail() {
		String emailId = "dummyEmailId@hcl.com";

		when(service.getUserByEmail(emailId)).thenReturn(null);
		boolean firstTimeSignIn = service.checkIsFirstTimeSignIn(emailId, "39475");
		assertEquals(false, firstTimeSignIn);
	}

	@Test
	public void checkUserEmailAndPasswordWithSuccess() {
		ArrayList<UserPassword> userPasswordList = new ArrayList<UserPassword>();

		userPassword.setUserPassword(user.getPassword());
		userPasswordList.add(userPassword);
		user.setUserPassword(userPasswordList);

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(user.getEmail())).thenReturn(user);
		when(bcryptEncoder.matches(user.getPassword(), user.getPassword())).thenReturn(true);

		boolean result = service.checkUserEmailAndPassword(user.getEmail(), user.getPassword());
		assertEquals(true, result);
	}

	@Test
	public void checkUserEmailAndPasswordWithFail() {
		ArrayList<UserPassword> userPasswordList = new ArrayList<UserPassword>();

		userPassword.setUserPassword(user.getPassword());
		userPasswordList.add(userPassword);
		user.setUserPassword(userPasswordList);

		user.setFirstTimeSignIn(true);
		when(userMapper.getUserByEmailAddress(user.getEmail())).thenReturn(null);
		when(bcryptEncoder.matches(user.getPassword(), user.getPassword())).thenReturn(true);

		boolean result = service.checkUserEmailAndPassword(user.getEmail(), user.getPassword());
		assertEquals(false, result);
	}

	@Test
	public void updatePasswordWithSuccess() {
		when(bcryptEncoder.encode("12345")).thenReturn("encoded_password");
		userPassword.setUserId(user.getId());
		userPassword.setUserPassword(bcryptEncoder.encode(user.getPassword()));
		userPassword.setTimeStamp("12:10:1000");
		when(userMapper.insertPassword(userPassword)).thenReturn(1);
		when(service.findUserById(59)).thenReturn(user);
		assertNotNull(service.updatePassword(user));
	}

	@Test
	public void updatePasswordWithFail() {
		when(bcryptEncoder.encode("12345")).thenReturn("encoded_password");
		userPassword.setUserId(1);
		userPassword.setUserPassword(bcryptEncoder.encode("12345"));
		userPassword.setTimeStamp("12:10:1000");
		when(userMapper.insertPassword(userPassword)).thenReturn(1);
		when(service.findUserById(59)).thenReturn(null);
		assertNull(service.updatePassword(user));
	}

}