package com.hcl.employee.management.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import javax.mail.internet.MimeMessage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.boot.test.autoconfigure.AutoConfigureMybatis;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;

import com.hcl.employee.management.EmployeeManagementApplication;
import com.hcl.employee.management.exception.UserNotFoundException;
import com.hcl.employee.management.model.Mail;
import com.hcl.employee.management.serviceimpl.EmailServiceImpl;

@AutoConfigureMybatis
@SpringBootTest(classes = EmployeeManagementApplication.class)
class EmailServiceImplTest {

	@Mock
	private Logger logger;
	@Mock
	private JavaMailSender emailSender;
	@InjectMocks
	private EmailServiceImpl emailService;
	private Mail userMail;

	@BeforeEach
	public void setUp() throws Exception {
		userMail = new Mail();
		userMail.setPwdUpdated("pwdUpdated");
		userMail.setSubject("subject");
		userMail.setTo("ajeet@ajeet.com");
		MockitoAnnotations.initMocks(this);
	}

	@Mock
	MimeMessage message;

	@Test
	void sendEmailTestSuccess() throws Exception {
		boolean result = false;

		when(emailSender.createMimeMessage()).thenReturn(message);
		doNothing().when(emailSender).send(message);
		result = emailService.sendEmail(userMail);
		assertTrue(result);
	}

	@Test
	void sendEmailTestFail() {
		when(emailSender.createMimeMessage()).thenReturn(message);
		doThrow(new MailException("MailException: ") {
		}).when(emailSender).send(message);
		assertThrows(UserNotFoundException.class, () -> emailService.sendEmail(userMail));

	}

}