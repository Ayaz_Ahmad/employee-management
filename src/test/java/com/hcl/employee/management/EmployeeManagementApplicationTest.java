package com.hcl.employee.management;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = EmployeeManagementApplication.class)
class EmployeeManagementApplicationTest {

	@Test
	void test() {
		EmployeeManagementApplication.main(new String[] {});
		assertTrue(true);
	}

}
