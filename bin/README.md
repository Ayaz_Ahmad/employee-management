# Spring Boot Application Employee Management-Project

Create below database tables and you’re good to go.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit 
* [Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications
* [MySQL](https://www.mysql.com/) - Open-Source Relational Database Management System
* [git](https://git-scm.com/) - Free and Open-Source distributed version control system 
* [Swagger](https://swagger.io/) - Open-Source software framework backed by a large ecosystem of tools that helps developers design, build, document, and consume RESTful Web services.

## External Tools Used

* [Postman](https://www.getpostman.com/) - API Development Environment (Testing Docmentation)

## TABLES 
- [x] User Password TABLE
- [x] User Table

## User Password TABLE
The User Password table has a particular user passwords.
-------------------------------------------------------------------------------

CREATE TABLE  user_password (
password_id int(10) unsigned NOT NULL auto_increment,
user_id int(10) unsigned NOT NULL,
user_password varchar(245) NOT NULL,
time_stamp varchar(245) NOT NULL,
PRIMARY KEY  (password_id),
KEY FK_userpassword_user (user_id),
CONSTRAINT FK_userpassword_user FOREIGN KEY (user_id) REFERENCES user (user_id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;


## User TABLE
The User table has a particular user properties.
-------------------------------------------------------------------------------

CREATE TABLE  user (
user_id int(10) unsigned NOT NULL auto_increment,
first_name varchar(45) NOT NULL,
last_name varchar(45) default NULL,
email_id varchar(45) NOT NULL,
mobile varchar(45) NOT NULL,
continent varchar(45) NOT NULL,
country varchar(45) NOT NULL,
state varchar(45) NOT NULL,
city varchar(45) NOT NULL,
zipCode varchar(45) NOT NULL,
organization varchar(45) NOT NULL,
isFirstTimeSignIn boolean,
PRIMARY KEY  (user_id),
UNIQUE KEY Index_2_email_uniq (email_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 

## UserRole TABLE
CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1; 


## Documentation

* [Swagger](http://localhost:8080/swagger-ui.html) - Documentation & Testing
-------------------------------------------------------------------------------

## UserDTO Model
-------------------------------------------------------------------------------

{
  "firstName": "Shashank",
  "lastName":"Saxena",
  "email" : "sashank@hcl.com",
  "mobile": "9847537893",
  "continent":"Asia",
  "country" : "India",
  "state" : "MP",
  "city" : "Agra",
  "zipCode" : 34653,
  "organization" : "TCS",
  "password" :"ayush099@",
  "confirmPassword":"ayush099@",
  "isFirstTimeSignIn": true
}